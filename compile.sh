#!/bin/sh
# in a container created from the official Marp Docker image, do
# 1. copy all files except *.md from slides/ to public/, preserving subpaths;
# 2. convert all *.md files in slides/ to HTML files in public/, preserving subpaths;
# The default entrypoint of this image runs  node /home/marp/.cli/marp-cli.js [ARGS]
#   so we remove it (--entrypoint="" i.e. override with empty)

COPY_FILE_CMD='F="{}"; NEWF=$(echo $F | sed "s/^slides/public/") && mkdir -vp $(dirname $NEWF) && cp -Ruv $F $NEWF' 
COPY_NON_MD_FILES_CMD="find slides -type f ! -name '*.md' -exec sh -c '$COPY_FILE_CMD' \\;"
PROCESS_MD_FILES_CMD="node /home/marp/.cli/marp-cli.js -I slides -o public"
docker run --rm -v $PWD:/home/marp/app/ -e LANG=$LANG --entrypoint=""  marpteam/marp-cli  sh -c "
  $COPY_NON_MD_FILES_CMD &&
  $PROCESS_MD_FILES_CMD
"
