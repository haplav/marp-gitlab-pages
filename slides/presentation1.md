---
marp: true
theme: uncover
headingDivider: 2
---

## Hello, I'm Marp!

Write and convert your presentation deck with just a plain Markdown!

## Watch and preview

* Marp supports watch mode and preview window.
* Just move your mouse to see options!

---

- Explicit new slide by `---`
- It stands for a horizontal rule in usual MarkDown.

---

<!-- backgroundColor: beige -->

- We changed the background color using a directive!
  ```markdown
  <!-- backgroundColor: beige -->
  ```
- This holds from now on. Return to default color with
  ```markdown
  <!-- backgroundColor: default -->
  ```
- Alternatively, this would apply the setting only on the current page (spot directive)
  ```markdown
  <!-- _backgroundColor: aqua -->
  ```
- Directives are special comments. They are ignored in usual MarkDown.

## Automatic division by headers

<!-- backgroundColor: default -->

We have here automatic division  
for level-1 and level-2 headers  
by specifying `headingDivider`  
in the front matter:

```markdown
---
marp: true
theme: uncover
headingDivider: 2
---
```

## Break line, MarkDown
```markdown
First line with no spaces after.
_The next line shouldn't be broken - but it is in Marp :disappointed:_

First line with two spaces after.  
And the next line.

First line with the HTML tag after.<br>
And the next line.

First line with the backslash after.\
And the next line.
```

## Break line, rendered

First line with no spaces after.
_The next line shouldn't be broken - but it is in Marp :disappointed:_

First line with two spaces after.  
And the next line.

First line with the HTML tag after.<br>
And the next line.

First line with the backslash after.\
And the next line.

## Bold

```markdown
I just love **bold text**.  
I just love __bold text__.  
Love**is**bold
```

I just love **bold text**.  
I just love __bold text__.  
Love**is**bold

## Italic

```markdown
Italicized text is the *cat's meow*.  
Italicized text is the _cat's meow_.  
A*cat*meow
```

Italicized text is the *cat's meow*.  
Italicized text is the _cat's meow_.  
A*cat*meow

## Bold and Italic

```markdown
This text is ***really important***.  
This text is ___really important___.  
This text is __*really important*__.  
This text is **_really important_**.  
This is really***very***important text.
```

This text is ***really important***.  
This text is ___really important___.  
This text is __*really important*__.  
This text is **_really important_**.  
This is really***very***important text.

## Unordered list 1 (`-`)

```markdown
- First item
- Second item
- Third item
- Fourth item
```

- First item
- Second item
- Third item
- Fourth item

## Unordered list 2 (`*`)

```markdown
* First item
* Second item
* Third item
  * 3.1
  * 3.2
  * 3.3
* Fourth item
```
---
* First item
* Second item
* Third item
  * 3.1
  * 3.2
  * 3.3
* Fourth item

## Unordered list 3 (mixed)

```markdown
- First item (`-`)
- Second item (`-`)
* Third item (`*`)
  - 3.1 (`-`)
  * 3.2 (`*`)
  * 3.3 (`*`)
* Fourth item (`*`)
```
---
- First item (`-`)
- Second item (`-`)
* Third item (`*`)
  - 3.1 (`-`)
  * 3.2 (`*`)
  * 3.3 (`*`)
* Fourth item (`*`)

## Ordered list 1

```markdown
1. First item
2. Second item
3. Third item
4. Fourth item
```
 
1. First item
2. Second item
3. Third item
4. Fourth item

## Ordered list 2

```markdown
1. First item
1. Second item
1. Third item
```

1. First item
1. Second item
1. Third item

## Table
| header | header |
| ------ | ------ |
| cell | cell |
| cell | cell |

## Indented code block

MarkDown
```markdown
    # This program prints Hello, world!

    print('Hello, world!')
```

rendered

    # This program prints Hello, world!

    print('Hello, world!')

## Fenced code block

MarkDown
````markdown
```
# This program prints Hello, world!

print('Hello, world!')
```
````

rendered
```
# This program prints Hello, world!

print('Hello, world!')
```

## Fenced code block with language

MarkDown
````markdown
```python
# This program prints Hello, world!

print('Hello, world!')
```
````

rendered
```python
# This program prints Hello, world!

print('Hello, world!')
```

## Picture
```markdown
![w:300px](presentation1/bread.jpg "bread")
```

![w:300px](presentation1/bread.jpg "bread")

## Background Picture
```markdown
![bg w:75% blur:5px](https://image_url.jpg)
```

<!-- _footer: picture from https://en.wikipedia.org/wiki/Wikipedia:Featured_pictures#/media/File:Cinnamomum_verum_spices.jpg -->

![bg w:75% blur:5px](https://upload.wikimedia.org/wikipedia/commons/thumb/d/de/Cinnamomum_verum_spices.jpg/1920px-Cinnamomum_verum_spices.jpg)

More about images here:  
https://marpit.marp.app/image-syntax

## MarkDown References

- https://www.markdownguide.org/basic-syntax/
- https://docs.gitlab.com/ee/user/markdown.html

# <!--fit--> :+1:
