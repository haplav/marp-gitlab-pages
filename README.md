## Example Marp GitLab Pages site

[![pipeline status](https://gitlab.com/haplav/marp-gitlab-pages/badges/master/pipeline.svg)](https://gitlab.com/haplav/marp-gitlab-pages/-/commits/master)

## How does it work
* Everything runs as a GitLab CI pipeline
* We convert MarkDown to HTML using [Marp CLI](https://github.com/marp-team/marp-cli)
  * we use the official [`marpteam/marp-cli` Docker image](https://hub.docker.com/r/marpteam/marp-cli/)
  * we convert MarkDown files in the `slides` directory to HTML files in the `public` directory
    * see [.gitlab-ci.yml](.gitlab-ci.yml)
    * we remove the default entrypoint [here](.gitlab-ci.yml#L3)
    * we copy non-MD files from `slides` to `public` [here](.gitlab-ci.yml#L10)
    * we run `node /home/marp/.cli/marp-cli.js -I slides -o public` as a script [here](.gitlab-ci.yml#L12)
* GitLab pages automatically publish files in `public` directory to  
  **https://haplav.gitlab.io/marp-gitlab-pages/**

## Learn more
* [GitLab Pages](https://pages.gitlab.io)
* [GitLab Pages official documentation](https://docs.gitlab.com/ce/user/project/pages/)
* [Marp](https://marp.app/)
* [marp-cli](https://github.com/marp-team/marp-cli)
* [marp-cli @ Docker Hub](https://hub.docker.com/r/marpteam/marp-cli/)
* [.gitlab-ci reference](https://docs.gitlab.com/ee/ci/yaml/README.html)
* [Overriding the entrypoint of an image](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#overriding-the-entrypoint-of-an-image)

